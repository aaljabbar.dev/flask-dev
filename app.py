from flask import Flask, render_template

app = Flask(__name__)


@app.route('/') # templating
def myindex():
    return render_template('main.html')

@app.route('/contact')
def myContact():
    return render_template('contact.html')

@app.route('/about')
def myAbout():
    return render_template('about.html')

@app.route('/loop')
def testLoop():
    number = [1,2,3,4,5,6,7,8,9,10]
    return render_template('index.html', value=number)

@app.route('/if')
def testIf():
    number = [1,2,3,4,5,6,7,8,9,10]
    suasana = 'senang'
    return render_template('index.html', value=number, suasana=suasana)

if __name__ == '__main__':
    app.run(debug=True)
